package com.example.article_management.repository;


import com.example.article_management.ArticleManagement;
import com.example.article_management.model.ArticleManagementM;
import com.example.article_management.service.ArticleManagementService;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Repository
public class ArticleManagementRepository {

    List<ArticleManagementM> am = new ArrayList<>();
    public ArticleManagementRepository(){
        for(int i=0;i<5;i++){
            ArticleManagementM article = new ArticleManagementM();
            Faker fk = new Faker();

            article.setId(i+1);
            article.setTitle(fk.book().title());
            article.setDescription(fk.book().genre());
            article.setProfile("http://localhost:8080/images/harryPotter.jpeg");
            am.add(article);
        }

    }
    public List<ArticleManagementM> getAllArticle(){
        return  am;
    }

    public void addArticle(ArticleManagementM articleManagementM){
        articleManagementM.setId(am.size()+1);
//        System.out.println("hi"+articleManagementM);
        am.add(articleManagementM);
    }

    public ArticleManagementM findArticleByID(int id){
        return am.stream().filter(article -> article.getId()==id).findFirst().orElseThrow();
    }

    public void deleteArticleById(int id){
            for(int i=0; i<am.size(); i++) {
                if (am.get(i).getId() == id) {
                    am.remove(i);
                    break;
                }
            }
    }

    public void updateArticleById(ArticleManagementM articleManagementM){
        am.get(articleManagementM.getId() -1 ).setTitle(articleManagementM.getTitle());
        am.get(articleManagementM.getId() -1).setDescription(articleManagementM.getDescription());
        am.get(articleManagementM.getId() -1).setProfile(articleManagementM.getProfile());
        am.get(articleManagementM.getId() -1).setFile(articleManagementM.getFile());
    }

//    public ArticleManagementM findId(int id){
//        for(var temp :am){
//            if(temp.getId() == id){
//                return temp;
//            }
//        }
//        return am.get(0);
//    }
}
