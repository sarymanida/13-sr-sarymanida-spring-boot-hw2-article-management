package com.example.article_management.model;


import lombok.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.constraints.NotEmpty;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class ArticleManagementM {
    private int id ;

    @NotEmpty(message = "Name cannot be empty..")
    private String title ;

   @NotEmpty(message = "Address cannot be empty..")
    private String description;
    private String profile;
    private MultipartFile file;

}
