package com.example.article_management;


import com.example.article_management.model.ArticleManagementM;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArticleManagement {

    List<ArticleManagementM> getAllArticleManagement ();

    void addAllArticleManagement(ArticleManagementM articleManagementM);
    ArticleManagementM findArticleById(int id);

    void deleteArticleById(int id);

    void updateArticle(ArticleManagementM articleManagementM);
//    ArticleManagementM findById(int id);
}
