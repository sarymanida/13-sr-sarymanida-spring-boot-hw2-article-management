package com.example.article_management.service;

import com.example.article_management.FileStorage;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class FileStorageService implements FileStorage {
    Path fileStorageLocation;

    FileStorageService(){
        fileStorageLocation= Paths.get("src/main/resources/images");
    }
    @Override
    public String saveFile(MultipartFile file) throws IOException {
        String filename = file.getOriginalFilename();

        if (filename.contains("..")){
            System.out.println("Wrong format of the file.");
            return null;
        }

        String[] fileParts =filename.split("\\.");

        //extension
        filename = UUID.randomUUID() + "."+ fileParts[1];
        // filename = 239402309ru2ojfoj42jlskdf.png
        // src/main/resources/images/239402309ru2ojfoj42jlskdf.png
        // resolvePath
        Path resolvedPath = fileStorageLocation.resolve(filename);
        Files.copy(file.getInputStream(),resolvedPath, StandardCopyOption.REPLACE_EXISTING);


        return filename;
    }
}
